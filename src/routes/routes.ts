import { BillController } from "../controllers/billController";
import { UserController } from "../controllers/userController";
const { check, validationResult } = require('express-validator/check');
export class Routes {

    /**
     * Declaração dos controllers usados nas rotas
    */
    public billController: BillController = new BillController();
    public userController: UserController = new UserController();

    /**
     * Declaração das rotas da API
    */
    public routes(app): void {

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });
    
    //Rota para criar novo usuário com validação
    app.post('/user', 
    [
        check('name',"O nome é obrigatório").not().isEmpty()
    ], (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }else{
            this.userController.addNewUser(req,res);
        }
    })

    //Rota para criar nova fatura com validação
    app.post('/bill', 
    [
        check('value')
        .not().isEmpty().withMessage('O valor é obrigatório')
        .isNumeric().withMessage('O valor precisa ser numérico'),

        check('paid')
        .not().isEmpty().withMessage('A informação sobre o pagamento é obrigatória')
        .isBoolean().withMessage('A informação sobre o pagamento ser booleano'),

        check('dueDate')
        .not().isEmpty().withMessage('A data de vencimento é obrigatória'),
        
        check('nameCompany')
        .not().isEmpty().withMessage('O nome da empresa é obrigatório')
        .isLength({ max: 300 }).withMessage('O nome da empresa é muito longo'),
        
        check('user')
        .not().isEmpty().withMessage('O usuário é obrigatório')

    ], (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }else{
            this.billController.addNewBill(req,res);
        }
    })

    // Buscar todas as faturas
    app.route('/bill')
    .get(this.billController.getAllBills);

    // Buscar todas as faturas
    app.route('/user')
    .get(this.userController.getAllUsers);
    
    // Buscar informações de uma fatura específica
    app.route('/bill/:billId')
   .get(this.billController.getBillWithID);
    
   //Atualiza informações de uma fatura específica
   app.route('/bill/:billId')
   .put(this.billController.updateBill);

   //Deleta uma determinada fatura
   app.route('/bill/:billId')
   .delete(this.billController.deleteBill);

    }
}