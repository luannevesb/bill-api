import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import { Routes } from "./routes/routes";

const expressValidator = require('express-validator');

class App {

    public app: express.Application;
    public router: Routes = new Routes();
    
    /**
     * URL DO SEU MONGO DB
     */
    public mongoUrl: string = 'mongodb://localhost/tilix-db';

    constructor() {
        this.app = express();
        this.config();
        this.router.routes(this.app);
        this.mongoSetup();     
    }

    private config(): void{
        // Configuração para suportar POST do tipo application/json 
        this.app.use(bodyParser.json());

        // Configuração para suportar POST do tipo x-www-form-urlencoded 
        this.app.use(bodyParser.urlencoded({ extended: false }));

        //Configuração para usar o EXPRESS-VALIDATOR
        this.app.use(expressValidator());
    }

    private mongoSetup(): void{
        mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl, {
            useNewUrlParser: true
          });    
    }

}

export default new App().app;