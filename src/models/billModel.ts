import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * Schema: Define as propriedades dos documentos do MongoDB.
 */
export const BillSchema = new Schema({
    value: {
        type: Number,
        required: true
      },
      paid: {
        type: Boolean,
        required: true
      },
      dueDate: {
        type: Date,
        required: true
      },
      nameCompany: {
        type: String,
        required: true,
        maxlength: 300
      },
      user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
      }
});