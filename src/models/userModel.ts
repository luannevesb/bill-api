import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * Schema: Define as propriedades dos documentos do MongoDB.
 */
export const UserSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true,
      maxlength: 100
    }
  });

