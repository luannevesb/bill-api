import * as mongoose from 'mongoose';
import { BillSchema } from '../models/billModel';
import { UserSchema } from '../models/UserModel';
import { Request, Response } from 'express';

const Bill = mongoose.model('Bill', BillSchema);
export class BillController{

    /**
     * Método que adiciona uma nova fatura
     * @param req Request
     * @param res Response
     */
    public addNewBill (req: Request, res: Response) {                
        let newBill = new Bill(req.body);
    
        newBill.save((err, bill) => {
            if(err){
                res.send(err);
            }    
            res.json(bill);
        });
    }

    /**
     * Método que busca todas as faturas
     * @param req Request
     * @param res Response
     */
    public getAllBills (req: Request, res: Response) {           
        Bill.find().populate('user').exec(function(err, bills){
            if(err){
                res.send(err);
            }
            res.json(bills);
        });
    }

    /**
     * Método que busca uma fatura específica
     * @param req Request
     * @param res Response
     */
    public getBillWithID (req: Request, res: Response) {           
        Bill.findById(req.params.billId).populate('user').exec(function(err, bill){
            if(err){
                res.send(err);
            }
            res.json(bill);
        });
    }

    /**
     * Método que atualiza uma determinada fatura
     * @param req Request
     * @param res Response
     */
    public updateBill (req: Request, res: Response) {           
        Bill.findOneAndUpdate({ _id: req.params.billId }, req.body, { new: true }, (err, bill) => {
            if(err){
                res.send(err);
            }
            res.json(bill);
        });
    }

    /**
     * Método que exclui uma determinada fatura
     * @param req Request
     * @param res Response
     */
    public deleteBill (req: Request, res: Response) {           
        Bill.remove({ _id: req.params.billId }, (err, bill) => {
            if(err){
                res.send(err);
            }
            res.json({ message: 'Fatura deletada com sucesso!'});
        });
    }
}