import * as mongoose from 'mongoose';
import { UserSchema } from '../models/userModel';
import { Request, Response } from 'express';

const { body } = require('express-validator/check')
const User = mongoose.model('User', UserSchema);

export class UserController{

    /**
     * Método que adiciona um novo usuário
     * @param req Request
     * @param res Response
     */
    public addNewUser (req: Request, res: Response) {                
        let newUser = new User(req.body);
        
        newUser.save((err, user) => {
            if(err){
                res.send(err);
            }    
            res.json(user);
        });
    }

    /**
     * Método que busca todos os usuários
     * @param req Request
     * @param res Response
     */
    public getAllUsers (req: Request, res: Response) {           
        User.find({}, (err, user) => {
            if(err){
                res.send(err);
            }
            res.json(user);
        });
    }

}