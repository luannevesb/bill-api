# Bill-api

### Tecnologias
* [Node v11.10.0](https://nodejs.org/en/download/current/)
* Express v4.16.4
* Typescript v3.4.3
* [MongoDB 4.0.8 Community](https://www.mongodb.com/download-center?jmp=nav#community)
* Mongoose v5.5.1

### Instalação
    > git clone https://luannevesb@bitbucket.org/luannevesb/bill-api.git

    > cd bill-api

    > npm install

### Execução
    # Inicializar o "MongoDB"
    # Obs: Inicialização possível através do MongoDB Compass

    # Inicia o Servidor (API)
    > npm run prod

