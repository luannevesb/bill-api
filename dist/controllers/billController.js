"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const billModel_1 = require("../models/billModel");
const Bill = mongoose.model('Bill', billModel_1.BillSchema);
class BillController {
    /**
     * Método que adiciona uma nova fatura
     * @param req Request
     * @param res Response
     */
    addNewBill(req, res) {
        let newBill = new Bill(req.body);
        newBill.save((err, bill) => {
            if (err) {
                res.send(err);
            }
            res.json(bill);
        });
    }
    /**
     * Método que busca todas as faturas
     * @param req Request
     * @param res Response
     */
    getAllBills(req, res) {
        Bill.find().populate('user').exec(function (err, bills) {
            if (err) {
                res.send(err);
            }
            res.json(bills);
        });
    }
    /**
     * Método que busca uma fatura específica
     * @param req Request
     * @param res Response
     */
    getBillWithID(req, res) {
        Bill.findById(req.params.billId).populate('user').exec(function (err, bill) {
            if (err) {
                res.send(err);
            }
            res.json(bill);
        });
    }
    /**
     * Método que atualiza uma determinada fatura
     * @param req Request
     * @param res Response
     */
    updateBill(req, res) {
        Bill.findOneAndUpdate({ _id: req.params.billId }, req.body, { new: true }, (err, bill) => {
            if (err) {
                res.send(err);
            }
            res.json(bill);
        });
    }
    /**
     * Método que exclui uma determinada fatura
     * @param req Request
     * @param res Response
     */
    deleteBill(req, res) {
        Bill.remove({ _id: req.params.billId }, (err, bill) => {
            if (err) {
                res.send(err);
            }
            res.json({ message: 'Fatura deletada com sucesso!' });
        });
    }
}
exports.BillController = BillController;
//# sourceMappingURL=billController.js.map