"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const userModel_1 = require("../models/userModel");
const { body } = require('express-validator/check');
const User = mongoose.model('User', userModel_1.UserSchema);
class UserController {
    /**
     * Método que adiciona um novo usuário
     * @param req Request
     * @param res Response
     */
    addNewUser(req, res) {
        let newUser = new User(req.body);
        newUser.save((err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
    /**
     * Método que busca todos os usuários
     * @param req Request
     * @param res Response
     */
    getAllUsers(req, res) {
        User.find({}, (err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
}
exports.UserController = UserController;
//# sourceMappingURL=userController.js.map