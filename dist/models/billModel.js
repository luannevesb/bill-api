"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
/**
 * Schema: Define as propriedades dos documentos do MongoDB.
 */
exports.BillSchema = new Schema({
    value: {
        type: Number,
        required: true
    },
    paid: {
        type: Boolean,
        required: true
    },
    dueDate: {
        type: Date,
        required: true
    },
    nameCompany: {
        type: String,
        required: true,
        maxlength: 300
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});
//# sourceMappingURL=billModel.js.map