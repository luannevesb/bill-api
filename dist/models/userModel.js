"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
/**
 * Schema: Define as propriedades dos documentos do MongoDB.
 */
exports.UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 100
    }
});
//# sourceMappingURL=UserModel.js.map