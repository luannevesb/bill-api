"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const routes_1 = require("./routes/routes");
const expressValidator = require('express-validator');
class App {
    constructor() {
        this.router = new routes_1.Routes();
        /**
         * URL DO SEU MONGO DB
         */
        this.mongoUrl = 'mongodb://localhost/tilix-db';
        this.app = express();
        this.config();
        this.router.routes(this.app);
        this.mongoSetup();
    }
    config() {
        // Configuração para suportar POST do tipo application/json 
        this.app.use(bodyParser.json());
        // Configuração para suportar POST do tipo x-www-form-urlencoded 
        this.app.use(bodyParser.urlencoded({ extended: false }));
        //Configuração para usar o EXPRESS-VALIDATOR
        this.app.use(expressValidator());
    }
    mongoSetup() {
        mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl, {
            useNewUrlParser: true
        });
    }
}
exports.default = new App().app;
//# sourceMappingURL=app.js.map